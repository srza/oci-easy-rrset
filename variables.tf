variable "zone_name" {
  type        = string
  description = "The DNS name of the zone"
}

variable "compartment_id" {
  type        = string
  description = "ID of OCI compartment"
}

variable "zone_id" {
  type        = string
  description = "ID of the zone to associate to the RRSET"
}

variable "rrdata" {
  type        = map(any)
  description = "A map of the complete rrset data for the zone"
}

variable "ttl" {
  type        = number
  default     = 3600
  description = "Default TTL for an item. Defaults to 3600"
}
