resource "oci_dns_rrset" "rrdata" {
  compartment_id  = var.compartment_id
  zone_name_or_id = var.zone_id
  domain          = var.zone_name

  for_each = var.rrdata

  rtype = upper(each.key)

  dynamic "items" {
    for_each = each.value

    content {
      ttl    = try(items.value.ttl, var.ttl)
      rdata  = items.value.data
      domain = var.zone_name
      rtype  = upper(each.key)
    }
  }
}