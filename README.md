# Easily create OCI RRSETs in Terraform

What is this? Well, if you've ever created a RRSET in terraform for more than a trivial domain, you'll understand the pain of verbosity - especially if you come from a 'pure' DNS world.

This module attempts to ease this pain by taking in a terraform map to recreate each rrset, rather than a repetitive list of items.

## Usage

To use this module, you could specify something like this:

```terraform
resource "oci_dns_zone" "example_zone" {
  compartment_id = var.compartment_id
  name = "example.com"
  zone_type = var.zone_type
}

module "example_zone_root" {
  source = "gitlab.com/srza/oci-easy-rrset/local"
  version = "0.0.1"

  zone_name = "example.com"
  zone_id = oci_dns_zone.example_zone.id
  compartment_id = var.compartment_id

  rrdata = {
    mx = {
      mx1 = {
        data = "10 mail1.example.com"
      }
      mx2 = {
        data = "20 mail2.example.com"
        ttl = 7200
      }
    }
    a = {
      a1 = {
        data = "192.0.2.5"
      }
    }
    txt = {
      txt1 = {
        data = "\"txt record\""
        ttl = 3600
      }
    }
  }
}

module "example_zone_www" {
  source = "gitlab.com/srza/oci-easy-rrset"
  version = "0.0.1"

  zone_name = "www.example.com"
  zone_id = oci_dns_zone.example_zone.id
  compartment_id = var.compartment_id

  rrdata = {
    a = {
      a1 = {
        data = "192.0.2.5"
      }
    }
  }
}
```

The same config as above without this module would be significantly larger - and difficult to fit on to one screen.

## Contributing

This Project is licensed under the MIT License, and your contributions would help it grow! Feel free to create an issue on the project, or alternatively, fork it and submit a Merge Request!
